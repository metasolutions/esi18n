import moment from 'moment';
import { isArray, isObject } from 'lodash-es';
import pluralRules from './pluralRules';

/**
 *  Template function that support both $ and ${} as delimiters
 *
 * @param {string} nlsString
 * @param {Object} attrs
 *
 * @returns {string}
 */
const template = (nlsString, attrs) => {
  let replacedNlsString = nlsString;
  Object.entries(attrs).forEach(([key, value]) => {
    replacedNlsString = replacedNlsString.replace(
      new RegExp(`\\$(\\{${key}\\}|${key})`, 'g'),
      value,
    );
  });
  return replacedNlsString;
};

/**
 * @param {(Object|string|number)} parameters
 *
 * @returns {Object}
 */
const normalizeParameters = (parameters) => {
  if (typeof parameters === 'string') {
    return { 1: parameters };
  }

  if (parameters === parseInt(parameters, 10)) {
    return { 1: parameters };
  }

  if (parameters.class) {
    parameters._class = parameters.class;
  }

  if (isArray(parameters)) {
    const obj = {};
    for (let i = 0; i < parameters.length; i++) {
      obj[`${i}`] = parameters[i];
    }
    return obj;
  }

  if (isObject(parameters)) {
    return parameters;
  }

  throw 'Parameters must be either an integer, an array or an object.';
};

/**
 * @param {string} str
 *
 * @returns {array}
 */
const splitLocalizedValue = (str) => {
  if (str == null) {
    console.warn('Invalid NLS String:', str);
    return str;
  }

  const cstr = str.replace(/\$(\d)/g, '\${$1}');
  let arr = cstr.split(/({{|}})/g);
  arr = arr.filter(item => (item !== '{{' && item !== '}}'));

  return arr.map((item, idx) => {
    if (idx % 2 === 1) {
      try {
        const obj = {};
        const plarr = item.split('|');
        const conf = plarr[0].split(':');
        obj.type = conf[0];
        if (conf.length === 2) {
          obj.variable = conf[1].replace(/\${(.*)}/, '$1');
        } else {
          obj.variable = '1';
        }
        plarr.shift();
        obj.forms = plarr;
        return obj;
      } catch (e) {
        throw `Syntax error in localization string: ${str}`;
      }
    } else {
      return item;
    }
  });
};

const joinLocalizedValue = (arr, language, parameters) => {
  const rule = pluralRules.findRule(language);
  const larr = arr.map((item) => {
    if (isObject(item)) {
      const pluralNumber = parseInt(parameters[item.variable], 10);

      return item.forms[rule(pluralNumber)];
    }

    return item;
  });
  return larr.join('');
};

/**
 * @param {Object} bundleFromLoader
 *
 * @returns {Object}
 */
const constructNLSHeirarchy = (bundleFromLoader) => {
  const rootLang = bundleFromLoader.filter(bundle => bundle.lang === 'root')[0];
  const nonRootLangs = bundleFromLoader.filter(bundle => bundle.lang !== 'root');

  // Dealing with our default language
  nonRootLangs.push({ lang: 'en', nls: {} });
  nonRootLangs.push({ lang: 'en-US', nls: {} });

  if (rootLang == null) {
    return null;
  }

  const componentNLSBundle = nonRootLangs.reduce((accum, nlsObject) => {
    accum[nlsObject.lang] = Object.create(rootLang.nls);
    // Assign the language-specific values to that object
    Object.assign(accum[nlsObject.lang], nlsObject.nls);

    return accum;
  }, {});

  return componentNLSBundle;
};

/**
 * @param {Object} bundle
 * @param {string} key
 *
 * @returns {string}
 */
const getNLSTemplate = (bundle, key) => (bundle[key] ? bundle[key] : '');

/**
 *
 * @param {string} nlsTemplateString
 * @param {Object} attrs
 *
 * @returns {String}
 */
const renderNLSTemplate = (nlsTemplateString, attrs) => {
  if (!nlsTemplateString) {
    return nlsTemplateString;
  }

  const normalizedParameters = attrs == null ? {} : normalizeParameters(attrs);
  let nlsString = joinLocalizedValue(
    splitLocalizedValue(nlsTemplateString),
    moment.locale(),
    normalizedParameters,
  );

  try {
    // Clean any instances of "class" parameters since this is a JS keyword
    if (normalizedParameters.class != null) {
      nlsString = nlsString.split('${class}').join('${_class}');
    }

    return template(nlsString, normalizedParameters);
  } catch (e) {
    console.warn(`Could not replace all variables in string: ${nlsString}.`);

    return nlsString;
  }
};

// Use the browser's preferred language as the default
const isBrowser = typeof window !== 'undefined';
if (isBrowser){
  moment.locale(window.navigator.language);
} else {
  moment.locale('en');
}

const esi18n = {
  /**
   *
   * @returns {string}
   */
  getLocale: () => moment.locale(),
  setLocale(locale) {
    this.set(locale);
  },
  normalizeLocale(locale) {
    return locale ? locale.toLowerCase() : this.getCurrentLocale();
  },
  /**
   * @param {Object} bundleFromLoader
   *
   * @returns {Object}
   */
  // getLocalization: (moduleName, bundleName, locale, onLoad) => { // OLD signature
  getLocalization(bundleFromLoader) {
    // Deal with any NLS overrides that were set
    if (this.overrides) {
      const bundleKey = bundleFromLoader[0].path;

      if (this.overrides[bundleKey]) {
        const bundleOverrides = this.overrides[bundleKey];
        const overridenBundle = bundleFromLoader.map((langBundle) => {
          const overrides = bundleOverrides.filter(bundle => bundle.lang === langBundle.lang);
          if (overrides.length > 0) {
            const override = overrides[0]; // Grab the first, hopefully there aren't more. Ignore the others in that case.
            langBundle.nls = { ...langBundle.nls, ...override.nls };
          }

          return langBundle;
        });

        return constructNLSHeirarchy(overridenBundle)[moment.locale()];
      }
    }

    return constructNLSHeirarchy(bundleFromLoader)[moment.locale()];
  },

  /**
   * @param {string} locale
   * @param {function} callback
   *
   * @returns {esi18n}
   */
  set(locale, onLocaleChange) {
    const oldlocale = this.getLocale();
    this.setCurrentDefaultLocale(locale);
    onLocaleChange && onLocaleChange();
    return this;
  },
  /**
   * Gets a date string and accepts options for formatting the date
   *
   * @param {Date|String} The date to be formatted
   * @param {object} options for how to format the date
   *
   * @returns {String}
   */
  getDate(date, formatOptions) {
    // choice of long, short, medium or full (plus any custom additions). Defaults to ‘short’
    // { selector: 'date', formatLength: 'medium' }
    // { selector: 'time', formatLength: 'short' }
    // { selector: 'date', datePattern: shortDateFormat }
    // { selector: 'time', formatLength: 'medium' }
    // { formatLength: 'full' }
    const parsedDate = moment(date);

    let format = 'lll';
    if (formatOptions) {
      if (formatOptions.datePattern) {
        format = formatOptions.datePattern;
      }
      else if (formatOptions.formatLength === 'full') {
        format = 'LLLL';
      }
      else {
        switch (formatOptions.selector) {
          case 'date':
            if (formatOptions.formatLength === 'short') {
              format = 'l';
            }
            else {
              format = 'LL';
            }
            break;
          case 'time':
            if (formatOptions.formatLength === 'short') {
              format = 'LT';
            }
            else if (formatOptions.formatLength === 'medium') {
              format = 'lll';
            }
            break;
          default:
            break;
        }
      }
    }

    return parsedDate.format(format);
  },
  /**
   * @param {string} locale
   *
   * @returns {string}
   */
  setCurrentDefaultLocale: locale => moment.locale(locale.toLowerCase()),

  // TODO @scazan Add this in based on defaults.js in commons
  setOverrides(overrides) {
    this.overrides = overrides;
  },

  renderNLSTemplate,
  /**
   * @param {Object} bundleFromLoader
   * @param {string} key
   * @param {Object} attrs
   *
   * @returns {undefined}
   */
  localize(bundleFromLoader, key, attrs) {
    const nlsString = renderNLSTemplate(
      getNLSTemplate(this.getLocalization(bundleFromLoader), key),
      attrs,
    );

    return nlsString;
  },
};

export default esi18n;
