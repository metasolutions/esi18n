/* global define,__confolio */

const rules = [
  {
    langs: 'af asa ast az bem bez bg brx cgg chr ckb dv ee el eo es eu fo fur fy gsw ha haw hu jgo jmc ka kaj kcg kk kkj kl ks ksb ku ky lb lg mas mgo ml mn nah nb nd ne nn nnh no nr ny nyn om or os pap ps rm rof rwk saq seh sn so sq ss ssy st syr ta te teo tig tk tn tr ts uz ve vo vun wae xh xog',
    rule: n => (n === 1 ? 0 : 1),
  },
  {
    langs: 'bm bo dz id ig ii in ja jbo jv jw kde kea km ko lkt lo ms my nqo root sah ses sg th to vi wo yo zh',
    rule: () => 0,
  },
  {
    langs: 'ak bh guw ln mg nso pa ti wa',
    rule: n => (n === 0 || n === 1 ? 0 : 1),
  },
  { // Same as above, but treated separately for some reason, investigate.
    langs: 'ff fr hy kab',
    rule: n => (n === 0 || n === 1 ? 0 : 1),
  },
  { // Same as above, but treated separately for some reason, investigate.
    langs: 'am bn fa gu hi kn mr zu',
    rule: n => (n === 0 || n === 1 ? 0 : 1),
  },
  {
    langs: 'iu kw naq se sma smi smj smn sms',
    rule: n => (n === 1 ? 0 : (n === 2 ? 1 : 2)),
  },
  {
    langs: 'ca de en et fi gl it ji nl sv sw ur yi',
    rule: n => (n === 1 ? 0 : 1),
  },
];

const obj = {};
rules.forEach((rule) => {
  rule.langs.split(' ').forEach((lang) => {
    obj[lang] = rule.rule;
  });
});

obj.findRule = (lang) => {
  if (lang != null && lang !== '' && lang !== 'root') {
    if (obj.hasOwnProperty(lang)) {
      return obj[lang];
    }
    const baselang = lang.split('_')[0].split('-')[0];
    if (obj.hasOwnProperty(baselang)) {
      return obj[baselang];
    }
  }
  // Fallback to english
  return obj.en;
};

export default obj;
