
const path = require('path');

module.exports = {
  watch: false,
  entry: './src/index.js',
  output: {
    filename: 'esi18n.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'esi18n',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  devtool: '#inline-source-map',
};
