const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'esi18n.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'esi18n',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env', {
                targets: { ie: 11 },
              },
              ],
            ],
            plugins: [
              '@babel/plugin-proposal-object-rest-spread'
            ],
          },
        },
      },
    ],
  },
  context: __dirname, // string (absolute path!)
};
